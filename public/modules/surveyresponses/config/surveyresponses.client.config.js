'use strict';

// Configuring the Articles module
angular.module('surveyresponses').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Surveyresponses', 'surveyresponses', 'dropdown', '/surveyresponses(/create)?');
		Menus.addSubMenuItem('topbar', 'surveyresponses', 'List Surveyresponses', 'surveyresponses');
		Menus.addSubMenuItem('topbar', 'surveyresponses', 'New Surveyresponse', 'surveyresponses/create');
	}
]);