'use strict';

//Setting up route
angular.module('surveyresponses').config(['$stateProvider',
	function($stateProvider) {
		// Surveyresponses state routing
		$stateProvider.
		state('listSurveyresponses', {
			url: '/surveyresponses',
			templateUrl: 'modules/surveyresponses/views/list-surveyresponses.client.view.html'
		}).
		state('createSurveyresponse', {
			url: '/surveyresponses/create',
			templateUrl: 'modules/surveyresponses/views/create-surveyresponse.client.view.html'
		}).
		state('thanks', {
			url: '/surveyresponses/thanks',
			templateUrl: 'modules/surveyresponses/views/thanks.client.view.html'
		}).
		state('viewSurveyresponse', {
			url: '/surveyresponses/:surveyresponseId',
			templateUrl: 'modules/surveyresponses/views/view-surveyresponse.client.view.html'
		}).
		state('editSurveyresponse', {
			url: '/surveyresponses/:surveyresponseId/edit',
			templateUrl: 'modules/surveyresponses/views/edit-surveyresponse.client.view.html'
		});
	}
]);