'use strict';

//Surveyresponses service used to communicate Surveyresponses REST endpoints
angular.module('surveyresponses').factory('Surveyresponses', ['$resource',
	function($resource) {
		return $resource('surveyresponses/:surveyresponseId', { surveyresponseId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);