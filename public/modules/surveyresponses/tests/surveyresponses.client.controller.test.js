'use strict';

(function() {
	// Surveyresponses Controller Spec
	describe('Surveyresponses Controller Tests', function() {
		// Initialize global variables
		var SurveyresponsesController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Surveyresponses controller.
			SurveyresponsesController = $controller('SurveyresponsesController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Surveyresponse object fetched from XHR', inject(function(Surveyresponses) {
			// Create sample Surveyresponse using the Surveyresponses service
			var sampleSurveyresponse = new Surveyresponses({
				name: 'New Surveyresponse'
			});

			// Create a sample Surveyresponses array that includes the new Surveyresponse
			var sampleSurveyresponses = [sampleSurveyresponse];

			// Set GET response
			$httpBackend.expectGET('surveyresponses').respond(sampleSurveyresponses);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.surveyresponses).toEqualData(sampleSurveyresponses);
		}));

		it('$scope.findOne() should create an array with one Surveyresponse object fetched from XHR using a surveyresponseId URL parameter', inject(function(Surveyresponses) {
			// Define a sample Surveyresponse object
			var sampleSurveyresponse = new Surveyresponses({
				name: 'New Surveyresponse'
			});

			// Set the URL parameter
			$stateParams.surveyresponseId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/surveyresponses\/([0-9a-fA-F]{24})$/).respond(sampleSurveyresponse);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.surveyresponse).toEqualData(sampleSurveyresponse);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Surveyresponses) {
			// Create a sample Surveyresponse object
			var sampleSurveyresponsePostData = new Surveyresponses({
				name: 'New Surveyresponse'
			});

			// Create a sample Surveyresponse response
			var sampleSurveyresponseResponse = new Surveyresponses({
				_id: '525cf20451979dea2c000001',
				name: 'New Surveyresponse'
			});

			// Fixture mock form input values
			scope.name = 'New Surveyresponse';

			// Set POST response
			$httpBackend.expectPOST('surveyresponses', sampleSurveyresponsePostData).respond(sampleSurveyresponseResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Surveyresponse was created
			expect($location.path()).toBe('/surveyresponses/' + sampleSurveyresponseResponse._id);
		}));

		it('$scope.update() should update a valid Surveyresponse', inject(function(Surveyresponses) {
			// Define a sample Surveyresponse put data
			var sampleSurveyresponsePutData = new Surveyresponses({
				_id: '525cf20451979dea2c000001',
				name: 'New Surveyresponse'
			});

			// Mock Surveyresponse in scope
			scope.surveyresponse = sampleSurveyresponsePutData;

			// Set PUT response
			$httpBackend.expectPUT(/surveyresponses\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/surveyresponses/' + sampleSurveyresponsePutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid surveyresponseId and remove the Surveyresponse from the scope', inject(function(Surveyresponses) {
			// Create new Surveyresponse object
			var sampleSurveyresponse = new Surveyresponses({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Surveyresponses array and include the Surveyresponse
			scope.surveyresponses = [sampleSurveyresponse];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/surveyresponses\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleSurveyresponse);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.surveyresponses.length).toBe(0);
		}));
	});
}());