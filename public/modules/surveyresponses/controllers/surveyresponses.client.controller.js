'use strict';

// Surveyresponses controller
angular.module('surveyresponses').controller('SurveyresponsesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Surveyresponses',
	function($scope, $stateParams, $location, Authentication, Surveyresponses) {
		$scope.authentication = Authentication;

		$scope.attract = false;
		$scope.entertain = false;
		$scope.engage = false;
		$scope.transact = false;
		$scope.reward = false;
		$scope.measure = false;

		$scope.clearResponses = function(){
			$scope.attract = false;
			$scope.entertain = false;
			$scope.engage = false;
			$scope.transact = false;
			$scope.reward = false;
			$scope.measure = false;
		};

		$scope.animated = function(){

		}

		// Create new Surveyresponse
		$scope.create = function() {
			// Create new Surveyresponse object
			var surveyresponse = new Surveyresponses ({
				// name: this.name,
				attract: this.attract ? 1 : 0,
				entertain: this.entertain ? 1 : 0,
				engage: this.engage ? 1 : 0,
				transact: this.transact ? 1 : 0,
				reward: this.reward ? 1 : 0,
				measure: this.measure ? 1 : 0,
			});


			// Redirect after save
			surveyresponse.$save(function(response) {
				// $location.path('surveyresponses/' + response._id);
				$location.path('surveyresponses/thanks');
				$scope.clearResponses();

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Surveyresponse
		$scope.remove = function(surveyresponse) {
			if ( surveyresponse ) { 
				surveyresponse.$remove();

				for (var i in $scope.surveyresponses) {
					if ($scope.surveyresponses [i] === surveyresponse) {
						$scope.surveyresponses.splice(i, 1);
					}
				}
			} else {
				$scope.surveyresponse.$remove(function() {
					$location.path('surveyresponses');
				});
			}
		};

		// Update existing Surveyresponse
		$scope.update = function() {
			var surveyresponse = $scope.surveyresponse;

			surveyresponse.$update(function() {
				$location.path('surveyresponses/' + surveyresponse._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.list = function(){
				// console.log($scope.authentication.user);
			if($scope.authentication.user == ''){
				$location.path('');
			}
		}

		// Find a list of Surveyresponses
		$scope.find = function() {
			$scope.surveyresponses = Surveyresponses.query();
		};

		// Find existing Surveyresponse
		$scope.findOne = function() {
			$scope.surveyresponse = Surveyresponses.get({ 
				surveyresponseId: $stateParams.surveyresponseId
			});
		};
	}
]);

setRandomClass();
setInterval(function () {
    setRandomClass();
}, 2000);

function setRandomClass() {
    var ul = $("#allbuttons");
    var items = ul.find("div");
    var number = items.length;
    var random = Math.floor((Math.random() * number));
    items.removeClass("animated");
    items.eq(random).addClass("animated");
}

$( "body" ).on( "click", "#ezesuite", function() {
	if($( "#ezesuite" ).hasClass('rotateIn')){
  	$( "#ezesuite" ).removeClass('rotateIn');
	} else {
  	$( "#ezesuite" ).addClass('rotateIn');
  	setTimeout(function(){
  		$( "#ezesuite" ).removeClass('rotateIn');
  	}, 1500)
	}
});

setTimeout(function(){
  		$( "#ezesuite" ).removeClass('rotateIn');
  	}, 1500);

$( "body" ).on( "click", ".buttons", function() {
	$(this).removeClass('animated')
});