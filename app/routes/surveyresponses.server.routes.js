'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var surveyresponses = require('../../app/controllers/surveyresponses.server.controller');

	// Surveyresponses Routes
	app.route('/surveyresponses')
		.get(surveyresponses.list)
		// .post(users.requiresLogin, surveyresponses.create);
		.post(surveyresponses.create);

	// Surveyresponses Routes
	app.route('/surveyresponses/downloadexcel')
		.get(surveyresponses.downloadexcel)

	app.route('/surveyresponses/:surveyresponseId')
		.get(surveyresponses.read)
		.put(users.requiresLogin, surveyresponses.hasAuthorization, surveyresponses.update)
		.delete(users.requiresLogin, surveyresponses.hasAuthorization, surveyresponses.delete);

	// Finish by binding the Surveyresponse middleware
	app.param('surveyresponseId', surveyresponses.surveyresponseByID);
};
