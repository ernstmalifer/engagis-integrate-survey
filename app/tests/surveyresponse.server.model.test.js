'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Surveyresponse = mongoose.model('Surveyresponse');

/**
 * Globals
 */
var user, surveyresponse;

/**
 * Unit tests
 */
describe('Surveyresponse Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			surveyresponse = new Surveyresponse({
				name: 'Surveyresponse Name',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return surveyresponse.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			surveyresponse.name = '';

			return surveyresponse.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Surveyresponse.remove().exec();
		User.remove().exec();

		done();
	});
});