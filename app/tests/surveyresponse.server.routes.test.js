'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Surveyresponse = mongoose.model('Surveyresponse'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, surveyresponse;

/**
 * Surveyresponse routes tests
 */
describe('Surveyresponse CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Surveyresponse
		user.save(function() {
			surveyresponse = {
				name: 'Surveyresponse Name'
			};

			done();
		});
	});

	it('should be able to save Surveyresponse instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Surveyresponse
				agent.post('/surveyresponses')
					.send(surveyresponse)
					.expect(200)
					.end(function(surveyresponseSaveErr, surveyresponseSaveRes) {
						// Handle Surveyresponse save error
						if (surveyresponseSaveErr) done(surveyresponseSaveErr);

						// Get a list of Surveyresponses
						agent.get('/surveyresponses')
							.end(function(surveyresponsesGetErr, surveyresponsesGetRes) {
								// Handle Surveyresponse save error
								if (surveyresponsesGetErr) done(surveyresponsesGetErr);

								// Get Surveyresponses list
								var surveyresponses = surveyresponsesGetRes.body;

								// Set assertions
								(surveyresponses[0].user._id).should.equal(userId);
								(surveyresponses[0].name).should.match('Surveyresponse Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Surveyresponse instance if not logged in', function(done) {
		agent.post('/surveyresponses')
			.send(surveyresponse)
			.expect(401)
			.end(function(surveyresponseSaveErr, surveyresponseSaveRes) {
				// Call the assertion callback
				done(surveyresponseSaveErr);
			});
	});

	it('should not be able to save Surveyresponse instance if no name is provided', function(done) {
		// Invalidate name field
		surveyresponse.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Surveyresponse
				agent.post('/surveyresponses')
					.send(surveyresponse)
					.expect(400)
					.end(function(surveyresponseSaveErr, surveyresponseSaveRes) {
						// Set message assertion
						(surveyresponseSaveRes.body.message).should.match('Please fill Surveyresponse name');
						
						// Handle Surveyresponse save error
						done(surveyresponseSaveErr);
					});
			});
	});

	it('should be able to update Surveyresponse instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Surveyresponse
				agent.post('/surveyresponses')
					.send(surveyresponse)
					.expect(200)
					.end(function(surveyresponseSaveErr, surveyresponseSaveRes) {
						// Handle Surveyresponse save error
						if (surveyresponseSaveErr) done(surveyresponseSaveErr);

						// Update Surveyresponse name
						surveyresponse.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Surveyresponse
						agent.put('/surveyresponses/' + surveyresponseSaveRes.body._id)
							.send(surveyresponse)
							.expect(200)
							.end(function(surveyresponseUpdateErr, surveyresponseUpdateRes) {
								// Handle Surveyresponse update error
								if (surveyresponseUpdateErr) done(surveyresponseUpdateErr);

								// Set assertions
								(surveyresponseUpdateRes.body._id).should.equal(surveyresponseSaveRes.body._id);
								(surveyresponseUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Surveyresponses if not signed in', function(done) {
		// Create new Surveyresponse model instance
		var surveyresponseObj = new Surveyresponse(surveyresponse);

		// Save the Surveyresponse
		surveyresponseObj.save(function() {
			// Request Surveyresponses
			request(app).get('/surveyresponses')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Surveyresponse if not signed in', function(done) {
		// Create new Surveyresponse model instance
		var surveyresponseObj = new Surveyresponse(surveyresponse);

		// Save the Surveyresponse
		surveyresponseObj.save(function() {
			request(app).get('/surveyresponses/' + surveyresponseObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', surveyresponse.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Surveyresponse instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Surveyresponse
				agent.post('/surveyresponses')
					.send(surveyresponse)
					.expect(200)
					.end(function(surveyresponseSaveErr, surveyresponseSaveRes) {
						// Handle Surveyresponse save error
						if (surveyresponseSaveErr) done(surveyresponseSaveErr);

						// Delete existing Surveyresponse
						agent.delete('/surveyresponses/' + surveyresponseSaveRes.body._id)
							.send(surveyresponse)
							.expect(200)
							.end(function(surveyresponseDeleteErr, surveyresponseDeleteRes) {
								// Handle Surveyresponse error error
								if (surveyresponseDeleteErr) done(surveyresponseDeleteErr);

								// Set assertions
								(surveyresponseDeleteRes.body._id).should.equal(surveyresponseSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Surveyresponse instance if not signed in', function(done) {
		// Set Surveyresponse user 
		surveyresponse.user = user;

		// Create new Surveyresponse model instance
		var surveyresponseObj = new Surveyresponse(surveyresponse);

		// Save the Surveyresponse
		surveyresponseObj.save(function() {
			// Try deleting Surveyresponse
			request(app).delete('/surveyresponses/' + surveyresponseObj._id)
			.expect(401)
			.end(function(surveyresponseDeleteErr, surveyresponseDeleteRes) {
				// Set message assertion
				(surveyresponseDeleteRes.body.message).should.match('User is not logged in');

				// Handle Surveyresponse error error
				done(surveyresponseDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Surveyresponse.remove().exec();
		done();
	});
});