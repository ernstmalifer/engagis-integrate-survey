'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Surveyresponse = mongoose.model('Surveyresponse'),
	nodeExcel = require('excel-export'),
	_ = require('lodash');

/**
 * Create a Surveyresponse
 */
exports.create = function(req, res) {
	var surveyresponse = new Surveyresponse(req.body);
	// surveyresponse.user = req.user;

	surveyresponse.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(surveyresponse);
		}
	});
};

/**
 * Show the current Surveyresponse
 */
exports.read = function(req, res) {
	res.jsonp(req.surveyresponse);
};

/**
 * Update a Surveyresponse
 */
exports.update = function(req, res) {
	var surveyresponse = req.surveyresponse ;

	surveyresponse = _.extend(surveyresponse , req.body);

	surveyresponse.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(surveyresponse);
		}
	});
};

/**
 * Delete an Surveyresponse
 */
exports.delete = function(req, res) {
	var surveyresponse = req.surveyresponse ;

	surveyresponse.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(surveyresponse);
		}
	});
};

/**
 * List of Surveyresponses
 */
exports.list = function(req, res) { 
	Surveyresponse.find().sort('-created').exec(function(err, surveyresponses) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(surveyresponses);
		}
	});
};

exports.downloadexcel = function(req, res){
	var conf ={};
  conf.cols = [];

  // Headers
  conf.cols.push({caption:'id', captionStyleIndex: 1, type:'string' });    
  conf.cols.push({caption:'attract', captionStyleIndex: 1, type:'string' });    
  conf.cols.push({caption:'entertain', captionStyleIndex: 1, type:'string' });    
  conf.cols.push({caption:'engage', captionStyleIndex: 1, type:'string' });    
  conf.cols.push({caption:'transact', captionStyleIndex: 1, type:'string' });    
  conf.cols.push({caption:'reward', captionStyleIndex: 1, type:'string' });    
  conf.cols.push({caption:'measure', captionStyleIndex: 1, type:'string' });    
  conf.cols.push({caption:'responded', captionStyleIndex: 1, type:'string' });    

  conf.rows = [];

  Surveyresponse.find().sort('-created').exec(function(err, surveyresponses) {
  	if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			surveyresponses.forEach(function(elem, index, array) {
				var row = [];
				var elemObj = elem.toObject();
		    console.log(elemObj._id ,elemObj.attract, elemObj.entertain, elemObj.engage, elemObj.transact, elemObj.reward, elemObj.measure, elemObj.created);

		    row.push(elemObj._id.toString());
		    row.push(elemObj.attract.toString());
		    row.push(elemObj.entertain.toString());
		    row.push(elemObj.engage.toString());
		    row.push(elemObj.transact.toString());
		    row.push(elemObj.reward.toString());
		    row.push(elemObj.measure.toString());
		    row.push(elemObj.created.toString());

		    conf.rows.push(row); 
			});

			var result = nodeExcel.execute(conf);
		  res.setHeader('Content-Type', 'application/vnd.openxmlformats');
		  res.setHeader("Content-Disposition", "attachment; filename=" + "Responses-" + new Date() + ".xlsx");
		  res.end(result, 'binary');
		}
  });
  // for (var j = 0; j < 1000; j++){
  //   var row = [];
  //   for (var k = 0; k < 100; k++){
  //     row.push(Math.random());  
  //   } 
  //   conf.rows.push(row); 
  //   conf.rows.push(row);    
  // }
  // var result = nodeExcel.execute(conf);
  // res.setHeader('Content-Type', 'application/vnd.openxmlformats');
  // res.setHeader("Content-Disposition", "attachment; filename=" + "Large.xlsx");
  // res.end(result, 'binary');  
};

/**
 * Surveyresponse middleware
 */
exports.surveyresponseByID = function(req, res, next, id) { 
	Surveyresponse.findById(id).populate('user', 'displayName').exec(function(err, surveyresponse) {
		if (err) return next(err);
		if (! surveyresponse) return next(new Error('Failed to load Surveyresponse ' + id));
		req.surveyresponse = surveyresponse ;
		next();
	});
};

/**
 * Surveyresponse authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.surveyresponse.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
