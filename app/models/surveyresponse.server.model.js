'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Surveyresponse Schema
 */
var SurveyresponseSchema = new Schema({
	// name: {
	// 	type: String,
	// 	default: '',
	// 	required: 'Please fill Surveyresponse name',
	// 	trim: true
	// },
	created: {
		type: Date,
		default: Date.now
	}
},{strict: false});

mongoose.model('Surveyresponse', SurveyresponseSchema);